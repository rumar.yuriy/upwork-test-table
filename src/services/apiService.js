import data from "../data/data.json";

const fetchData = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(data);
    }, 2000);
  });
};

export { fetchData };
